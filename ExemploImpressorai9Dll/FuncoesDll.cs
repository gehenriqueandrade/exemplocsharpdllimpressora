﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ExemploImpressorai9Dll
{
    class FuncoesDll
    {

        [DllImport("E1_Impressora.dll", EntryPoint = "AbreConexaoImpressora", CallingConvention = CallingConvention.StdCall)]
        public static extern int AbreConexaoImpressora(int tipo, String modelo, String conexao, int parametro);

        [DllImport("E1_Impressora.dll", EntryPoint = "FechaConexaoImpressora", CallingConvention = CallingConvention.StdCall)]
        public static extern int FechaConexaoImpressora();

        [DllImport("E1_Impressora.dll", EntryPoint = "ImpressaoTexto", CallingConvention = CallingConvention.StdCall)]
        public static extern int ImpressaoTexto(String dados, int posicao, int stilo, int tamanho);

        [DllImport("E1_Impressora.dll", EntryPoint = "Corte", CallingConvention = CallingConvention.StdCall)]
        public static extern int Corte(int avanco);

        [DllImport("E1_Impressora.dll", EntryPoint = "ImpressaoQRCode", CallingConvention = CallingConvention.StdCall)]
        public static extern int ImpressaoQRCode(String dados, int tamanho, int nivelCorrecao);

        [DllImport("E1_Impressora.dll", EntryPoint = "ImpressaoCodigoBarras", CallingConvention = CallingConvention.StdCall)]
        public static extern int ImpressaoCodigoBarras(int tipo, String dados, int altura, int largura, int HRI);

        [DllImport("E1_Impressora.dll", EntryPoint = "AvancaPapel", CallingConvention = CallingConvention.StdCall)]
        public static extern int AvancaPapel(int linhas);

        [DllImport("E1_Impressora.dll", EntryPoint = "LimpaBuffer", CallingConvention = CallingConvention.StdCall)]
        public static extern int LimpaBuffer();

        [DllImport("E1_Impressora.dll", EntryPoint = "StatusImpressora", CallingConvention = CallingConvention.StdCall)]
        public static extern int StatusImpressora(int param);

        [DllImport("E1_Impressora.dll", EntryPoint = "AbreGaveta", CallingConvention = CallingConvention.StdCall)]
        public static extern int AbreGaveta();

        [DllImport("E1_Impressora.dll", EntryPoint = "InicializaImpressora", CallingConvention = CallingConvention.StdCall)]
        public static extern int InicializaImpressora();

        [DllImport("E1_Impressora.dll", EntryPoint = "DefinePosicao", CallingConvention = CallingConvention.StdCall)]
        public static extern int DefinePosicao(int posicao);

        [DllImport("E1_Impressora.dll", EntryPoint = "TesteImpressora", CallingConvention = CallingConvention.StdCall)]
        public static extern int TesteImpressora();

        [DllImport("E1_Impressora.dll", EntryPoint = "ImprimeXMLSAT", CallingConvention = CallingConvention.StdCall)]
        public static extern int ImprimeXMLSAT(String dados);

        [DllImport("E1_Impressora.dll", EntryPoint = "ImprimeXMLCancelamentoSAT", CallingConvention = CallingConvention.StdCall)]
        public static extern int ImprimeXMLCancelamentoSAT(String dados, String assQRCode);

        [DllImport("E1_Impressora.dll", EntryPoint = "ImprimeXMLNFCe", CallingConvention = CallingConvention.StdCall)]
        public static extern int ImprimeXMLNFCe(String dados);

    }
}
