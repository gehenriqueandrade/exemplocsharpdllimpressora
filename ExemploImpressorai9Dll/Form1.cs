﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExemploImpressorai9Dll
{
    public partial class Form1 : Form
    {
        int retorno, tipo, baudRate;
        String modelo, conexao;

        // BOTÃO CÓDIGOS DE BARRAS.
        private void button4_Click(object sender, EventArgs e)
        {
            retorno = FuncoesDll.InicializaImpressora();
            retorno = FuncoesDll.ImpressaoTexto("Todos os Codigos.", 1, 8, 0);
            retorno = FuncoesDll.AvancaPapel(4);

            retorno = FuncoesDll.ImpressaoTexto("UPC-A", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoCodigoBarras(0, "01234567890", 90, 2, 4);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("UPC-E", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoCodigoBarras(1, "01234567890", 90, 2, 4);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("JAN 13 / EAN 13", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoCodigoBarras(2, "001234567890", 90, 2, 4);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("JAN 8 / EAN 8", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoCodigoBarras(3, "01234567", 90, 2, 4);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("CODEBAR", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoCodigoBarras(6, "A1234+5678B", 90, 2, 4);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("CODE 39", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoCodigoBarras(4, "*0123456789*", 90, 2, 4);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("CODE 93", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoCodigoBarras(4, "*TESTECODE93*", 90, 2, 4);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("ITF", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoCodigoBarras(5, "1234567890", 90, 2, 4);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("CODE 128", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            byte[] eu = new byte[] { 123, 67, 01, 02, 03, 04, 05, 06, 07, 01, 02, 03, 04, 05, 06, 07, 01, 02, 03, 04, 05, 06, 07, 01, 02, 03, 04, 05, 06, 07 };
            String retb = System.Text.Encoding.UTF8.GetString(eu);
            retorno = FuncoesDll.ImpressaoCodigoBarras(8, retb, 80, 2, 4);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("QR CODE", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            String qrcodee = "http://homnfce.sefaz.am.gov.br/nfceweb/consultarNFCe.jsp?chNFe=13161104240370000742652000000445901000445906&nVersao=100&tpAmb=2&dhEmi=323031362D31312D31375430393A33373A31372D30323A3030&vNF=11.98&vICMS=0.00&digVal=497A762B634A4439374258533568546751753541375330554E436B3D&cIdToken=000001&cHashQRCode=2A1303EDAEFAB7345F841BDC7A6BD75B1DD0866E";
            retorno = FuncoesDll.ImpressaoQRCode(qrcodee, 5, 2);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.Corte(1);
            if(retorno == 0)
            {
                textBox1.Text = "Impressão Realizada Com Sucesso.";
            }
            else
            {
                textBox1.Text = "Algo deu errado. Por favor, verifique o código.";
            }
        }
        // BOTÃO PARA ABRIR GAVETA.
        private void button6_Click(object sender, EventArgs e)
        {
            retorno = FuncoesDll.InicializaImpressora();
            retorno = FuncoesDll.AbreGaveta();
            if (retorno == 0)
            {
                textBox1.Text = "Gaveta Aberta com Sucesso.";
            }
            else
            {
                textBox1.Text = "Algo deu errado. Por favor, verifique o código.";
            }
        }
        // BOTÃO DE STATUS.
        private void button5_Click(object sender, EventArgs e)
        {
            retorno = FuncoesDll.StatusImpressora(4);
            switch (retorno)
            {
               case 0:
                    textBox1.Text = "Status de Impressora: Funcionalidade Total." + System.Environment.NewLine + "Status de Gaveta: Gaveta Fechada." + System.Environment.NewLine + "Status de Papel: Papel Suficiente.";    
                    break;
               case 1:
                    textBox1.Text = "Status de Impressora: Funcionalidade Total." + System.Environment.NewLine + "Status de Gaveta: Gaveta Aberta." + System.Environment.NewLine + "Status de Papel: Papel Suficiente.";
                    break;
                case 2:
                    textBox1.Text = "Status de Impressora: Tampa da Impressora Aberta." + System.Environment.NewLine + "Status de Gaveta: Gaveta Fechada." + System.Environment.NewLine + "Status de Papel: Papel Suficiente.";
                    break;
                case 3:
                    textBox1.Text = "Status de Impressora: Tampa da Impressora Aberta." + System.Environment.NewLine + "Status de Gaveta: Gaveta Aberta." + System.Environment.NewLine + "Status de Papel: Papel Suficiente.";
                    break;
                case 4:
                    textBox1.Text = "Status de Impressora: Funcionalidade Total." + System.Environment.NewLine + "Status de Gaveta: Gaveta Fechada." + System.Environment.NewLine + "Status de Papel: Pouco Papel.";
                    break;
                case 5:
                    textBox1.Text = "Status de Impressora: Funcionalidade Total." + System.Environment.NewLine + "Status de Gaveta: Gaveta Aberta." + System.Environment.NewLine + "Status de Papel: Pouco Papel.";
                    break;
                case 12:
                    textBox1.Text = "Status de Impressora: Funcionalidade Total." + System.Environment.NewLine + "Status de Gaveta: Gaveta Fechada." + System.Environment.NewLine + "Status de Papel: Fim de Papel.";
                    break;
                case 13:
                    textBox1.Text = "Status de Impressora: Funcionalidade Total." + System.Environment.NewLine + "Status de Gaveta: Gaveta Aberta." + System.Environment.NewLine + "Status de Papel: Fim de Papel.";
                    break;
                default:
                    textBox1.Text = "Resultado: " + retorno + System.Environment.NewLine + System.Environment.NewLine + "Erro não Registrado. Valor verificar.";
                    break;
            }
        }
        // BOTÃO CUPOM PADRÃO.
        private void button3_Click(object sender, EventArgs e)
        {
            retorno = FuncoesDll.InicializaImpressora();
            retorno = FuncoesDll.ImpressaoTexto("Elgin Manaus", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("Elgin S/A", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("Rua Abiurana, 579 Distrito Industrial Manaus - AM", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("CNPJ 14.200.166/0001-66 IE 111.111.111.111 IM", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("----------------------------------------------------------------", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("Extrato No 000000", 1, 8, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("CUPOM FISCAL ELETRONICO - SAT", 1, 8, 0);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("= T E S T E =", 1, 8, 0);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("----------------------------------------------------------------", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("CPF/CNPJ do Consumidor:", 0, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("----------------------------------------------------------------", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("# | COD | DESC | QTD | UN | VL UN R$ | (VL TR R$)* | VL ITEM R$", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("----------------------------------------------------------------", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("001 000000000000 Impressora i9 0 cx X 0,00 (0,00)           0,00", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("rateio de desconto sobre subtotal:                         -0,00", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("001 000000000000 Impressora i7 0 cx X 0,00 (0,00)           0,00", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("rateio de desconto sobre subtotal:                         -0,00", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("Total bruto de itens                                        0,00", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("Desconto sobre subtotal                                    -0,00", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("TOTAL R$                                   00,00", 0, 8, 0);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("Cartao de Debito                                           00,00", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("*ICMS a ser recolhido conforme LC 000/0000  -  Simples Nacional*", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("----------------------------------------------------------------", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("OBSERVACOES DO CONTRIBUINTE", 0, 1, 0);
            retorno = FuncoesDll.AvancaPapel(2);
            retorno = FuncoesDll.ImpressaoTexto("*Valor aproximado dos tributos do item", 0, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("Valor aproximado dos tributos deste cupom R$                0,00", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("(conforme Lei Fed 12.741/2012)", 0, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("----------------------------------------------------------------", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("SAT No. 000.000.000", 1, 8, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("00/00/0000 - 00:00:00", 1, 8, 0);
            retorno = FuncoesDll.AvancaPapel(2);
            String code128 = "{C35150661099008000141593515066109900800014159";
            retorno = FuncoesDll.ImpressaoCodigoBarras(8, code128, 80, 2, 1);
            retorno = FuncoesDll.AvancaPapel(2);
            String qrcode = "35141146377222003730599000004630000853254753|20141105134843|637.00||H0iMysWj\r\n" +
                    "M9zOXjaxkpPjqk7Q0Fp4RFvWkC0jLngU8o/pg5WpjhiVU2i/7BnIDz/WU3bMd9Cg6qWHvyn11e+\r\n" +
                    "qE7VpUjfZFPiczrrxPOqke3kdaX3y2db0/xebRcLUn1+PO2fvKK/7tAPMUiXvb1YDH6DqHVBhoM5A\r\n" +
                    "7u3P5RZrHvl1dfaJumLbe5uejKmq4rDNdOI5EVReJ5q1O6asOzwd8O0JzUGIf9aSdcClKWY3Xqt+0I\r\n" +
                    "67uo/hUr8tLMb2K15SIqoCBfya6sUIvA==";
            retorno = FuncoesDll.ImpressaoQRCode(qrcode, 5, 2);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("Consulte o QR Code pelo aplicativo \"De olho na nota\", ", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(1);
            retorno = FuncoesDll.ImpressaoTexto("disponivel na AppStore (Apple) e PlayStore (Android)", 1, 1, 0);
            retorno = FuncoesDll.AvancaPapel(3);
            retorno = FuncoesDll.Corte(1);
            if (retorno == 0)
            {
                textBox1.Text = "Impressão Realizada Com Sucesso.";
            }
            else
            {
                textBox1.Text = "Algo deu errado. Por favor, verifique o código.";
            }
        }
        // FORM PRINCIPAL.
        public Form1()
        {
            InitializeComponent();
        }
        // BOTÃO DE CONEXÃO.
        private void button2_Click(object sender, EventArgs e)
        {
            // Tratando Modelo.
            if (comboBox2.Text == "• Impressora i9")
            {
                modelo = "i9";
            }
            else if (comboBox2.Text == "• Impressora i7")
            {
                modelo = "i7";
            }
            else if (comboBox2.Text == "• Impressora i10")
            {
                modelo = "i10";
            }
            else if (comboBox2.Text == "• Impressora Fitpos")
            {
                modelo = "Fitpos";
            }
            else if (comboBox2.Text == "• Impressora BK-T681")
            {
                modelo = "BK-T681";
            }
            else if (comboBox2.Text == "• Impressora K")
            {
                modelo = "K";
            }
            else
            {
                MessageBox.Show("O campo \"Modelo\" esta incorreto. Favor verificar.");
            }

            // Tratando Conexão.
            if (comboBox1.Text == "• Comunicação Serial")
            {
                tipo = 2;
                conexao = textBox2.Text;
                baudRate = Convert.ToInt32(textBox3.Text);
            }
            else
            {
                tipo = 1;
                conexao = "USB";
                baudRate = 0;
            }

            retorno = FuncoesDll.AbreConexaoImpressora(tipo, modelo, conexao, baudRate);
            switch (retorno)
            {
                case 0:
                    label4.Text = "STATUS DE CONEXÃO: PORTA ABERTA COM SUCESSO.";
                    break;
                case -9999:
                    label4.Text = "STATUS DE CONEXÃO: ERRO DESCONHECIDO.";
                    break;
                case -2:
                    label4.Text = "STATUS DE CONEXÃO: TIPO DE CONEXÃO INVÁLIDA.";
                    break;
                case -3:
                    label4.Text = "STATUS DE CONEXÃO: MODELO INVÁLIDO.";
                    break;
                case -4:
                    label4.Text = "STATUS DE CONEXÃO: PORTA DE CONEXÃO FECHADA.";
                    break;
                case -5:
                    label4.Text = "STATUS DE CONEXÃO: CONEXÃO NEGADA.";
                    break;
                case -11:
                    label4.Text = "STATUS DE CONEXÃO: BAUDRATE INVÁLIDO.";
                    break;
                case 1:
                    label4.Text = "STATUS DE CONEXÃO: DISPOSITIVO NÃO EXISTE.";
                    break;
                case 2:
                    label4.Text = "STATUS DE CONEXÃO: DISPOSITIVO EM PROCESSO.";
                    break;
                case 7:
                    label4.Text = "STATUS DE CONEXÃO: ERRO NA ESCRITA DA PORTA";
                    break;
                case 8:
                    label4.Text = "STATUS DE CONEXÃO: ERRO NA LEITURA DA PORTA.";
                    break;
                case 11:
                    label4.Text = "STATUS DE CONEXÃO: ERRO DESCONHECIDO NA PORTA SERIAL.";
                    break;
                case -21:
                    label4.Text = "STATUS DE CONEXÃO: DISPOSITIVO NÃO ENCONTRADO.";
                    break;
                case -22:
                    label4.Text = "STATUS DE CONEXÃO: ERRO NA ABERTURA DA PORTA USB.";
                    break;
                case -23:
                    label4.Text = "STATUS DE CONEXÃO: ERRO DE CLAIM UL";
                    break;
                case -24:
                    label4.Text = "STATUS DE CONEXÃO: ERRO NA ESCRITA DA PORTA";
                    break;
                case -31:
                    label4.Text = "STATUS DE CONEXÃO: ERRO NA CONEXÃO TCP";
                    break;
                default:
                    label4.Text = "STATUS DE CONEXÃO: FAVOR ENTRAR EM CONTATO COM A ELGIN.";
                    break;
            }
        }
    }
}
